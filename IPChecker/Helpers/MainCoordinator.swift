//
//  MainCoordinator.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewModel = IPCheckerViewModel()
        let viewController = IPCheckerViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.pushViewController(viewController, animated: false)
    }
}
