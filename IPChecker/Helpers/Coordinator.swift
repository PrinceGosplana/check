//
//  Coordinator.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
