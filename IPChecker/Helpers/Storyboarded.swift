//
//  Storyboarded.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import UIKit

enum StoryboardName: String {
  case main                = "Main"
}


protocol Storyboarded {
    static func instantiate() -> Self
    static var storyboardName: StoryboardName { get set }
}

extension Storyboarded where Self: UIViewController & ViewModelBased {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: id) as! Self
        return viewController
    }
}
