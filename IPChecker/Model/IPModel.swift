//
//  IPModel.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import Foundation

public struct IPModel: Equatable, Codable {
   
    let country: String
    let countryCode: String
    let region: String
    let regionName: String
    let city: String
    let zip: String
    
    enum CodingKeys: String, CodingKey {
        case country
        case countryCode
        case region
        case regionName
        case city
        case zip
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(country, forKey: .country)
        try container.encode(countryCode, forKey: .countryCode)
        try container.encode(region, forKey: .region)
        try container.encode(regionName, forKey: .regionName)
        try container.encode(city, forKey: .city)
        try container.encode(zip, forKey: .zip)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        country = try container.decode(String.self, forKey: .country)
        countryCode = try container.decode(String.self, forKey: .countryCode)
        region = try container.decode(String.self, forKey: .region)
        regionName = try container.decode(String.self, forKey: .regionName)
        city = try container.decode(String.self, forKey: .city)
        zip = try container.decode(String.self, forKey: .zip)
    }
}
