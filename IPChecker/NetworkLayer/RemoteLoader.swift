//
//  RemoteLoader.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import Foundation

public enum HTTPClientResult {
    case success(Data)
    case failure(Error)
}

public enum Error: Swift.Error {
    case badURL
    case connectivity
    case invalidData
    case invalidDecodeData
}

public protocol HTTPClient {
    func get(completion: @escaping (HTTPClientResult) -> ())
}

class IPClient: HTTPClient {
    
    let url: URL?
    
    internal init(ipAdress: String) {
        let stringURL = String("http://ip-api.com/json/\(ipAdress)?fields=country,countryCode,region,regionName,city,zip&lang=ru")
        self.url = URL(string: stringURL)
    }
    
    func get(completion: @escaping (HTTPClientResult) -> ()) {
        guard let url = url else {
            completion(.failure(.badURL))
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, taskError in
            
            guard let httpResponse = response as? HTTPURLResponse, (200..<300).contains(httpResponse.statusCode) else {
                completion(.failure(.connectivity))
                return
            }
            
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            
            completion(.success(data))
        }.resume()
    }
    
}


public final class RemoteLoader {
    private let client: HTTPClient
    
    public enum Result: Equatable {
        case success(IPModel)
        case failure(Error)
    }
    
    public init(client: HTTPClient) {
        self.client = client
    }
    
    public func load(completion: @escaping (Result) -> () = { _ in }) {
        client.get() { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                guard let model = try? decoder.decode(IPModel.self, from: data) else {
                    completion(.failure(.invalidDecodeData))
                    return
                }
                completion(.success(model))
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
}
