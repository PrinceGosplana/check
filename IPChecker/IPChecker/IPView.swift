//
//  IPView.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 23.12.2020.
//

import UIKit

private extension CGFloat {
    static let cornerRadius: CGFloat = 20.0
    static let labelFontSize: CGFloat = 14.0
}

class IPView: UIView {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.placeholder = "Введите IP адресс"
            textField.autocorrectionType = .no
            textField.keyboardType = .numbersAndPunctuation
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.systemFont(ofSize: .labelFontSize, weight: .medium)
            titleLabel.textAlignment = .left
            titleLabel.tintColor = .darkText
        }
    }
    @IBOutlet weak var resultLabel: UILabel! {
        didSet {
            resultLabel.font = UIFont.systemFont(ofSize: .labelFontSize, weight: .regular)
            resultLabel.textAlignment = .left
            resultLabel.tintColor = .darkGray
            resultLabel.numberOfLines = 0
        }
    }
    @IBOutlet weak var doneButton: UIButton! {
        didSet {
            doneButton.backgroundColor = .systemBlue
            doneButton.clipsToBounds = true
            doneButton.layer.cornerRadius = .cornerRadius
            doneButton.tintColor = .white
            doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        }
    }
    
    @IBOutlet weak var buttonBottomOffset: NSLayoutConstraint!
    
    // MARK: - Function
    
    func inactiveState() {
        doneButton.setTitle("Поиск", for: .normal)
        titleLabel.text = "Данные о локации:"
        titleLabel.textColor = .lightText
        resultLabel.text = ""
        doneButton.isEnabled = false
        doneButton.backgroundColor = .systemGray
    }
    
    func makeRequestState() {
        doneButton.setTitle("Поиск", for: .normal)
        titleLabel.text = "Данные о локации:"
        titleLabel.textColor = .lightText
        resultLabel.text = ""
        doneButton.isEnabled = false
        doneButton.backgroundColor = .systemGray
    }
    
    func showResultState(result: String) {
        doneButton.setTitle("Поиск", for: .normal)
        titleLabel.text = "Данные о локации:"
        titleLabel.textColor = .darkText
        resultLabel.text = result
        doneButton.isEnabled = true
        doneButton.backgroundColor = .systemBlue
    }
    
    func errorState() {
        doneButton.setTitle("Повторить поиск", for: .normal)
        titleLabel.text = "Ошибка, повторите поиск"
        titleLabel.textColor = .darkText
        resultLabel.text = ""
        doneButton.isEnabled = true
        doneButton.backgroundColor = .systemRed
    }
    
    func readyState() {
        doneButton.setTitle("Поиск", for: .normal)
        titleLabel.text = "Данные о локации:"
        titleLabel.textColor = .darkText
        resultLabel.text = ""
        doneButton.isEnabled = true
        doneButton.backgroundColor = .systemBlue
    }
    
}
