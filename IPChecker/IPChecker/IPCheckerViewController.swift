//
//  IPCheckerViewController.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import UIKit

private extension CGFloat {
    static let bottomButtonOffset: CGFloat = 20.0
}

class IPCheckerViewController: UIViewController, ViewModelBased {

    typealias ViewModel = IPCheckerViewModel
    var viewModel: ViewModel?
    
    // MARK: - IBOutlets
    
    @IBOutlet private var ipView: IPView!
    
    // MARK: - IBAction
    
    @IBAction func tapSearch(_ sender: Any) {
        viewModel?.load()
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
        setBinding()
        ipView.textField.delegate = self
        ipView.inactiveState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupKeyboardObservers()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObservers() 
    }
    
    // MARK: - Objc
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            ipView.buttonBottomOffset.constant = .bottomButtonOffset + keyboardHeight
            UIView.animate(withDuration: TimeInterval.infinity) { self.view.layoutIfNeeded() }
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
            ipView.buttonBottomOffset.constant = .bottomButtonOffset
            UIView.animate(withDuration: TimeInterval.infinity) { self.view.layoutIfNeeded() }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        ipView.textField.resignFirstResponder()
    }
    
    // MARK: - Private functions
    
    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        ipView.addGestureRecognizer(tap)
    }
    
    private func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    private func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    private func setBinding() {
        viewModel?.inactiveState = { [weak self] in
            self?.ipView.inactiveState()
        }
        viewModel?.readyState =  { [weak self] in self?.ipView.readyState() }
        viewModel?.makeRequestState =  { [weak self] in self?.ipView.makeRequestState() }
        viewModel?.showResultState =  { [weak self] result in self?.ipView.showResultState(result: result) }
        viewModel?.errorState =  { [weak self] in  self?.ipView.errorState() }
    }
    
}

extension IPCheckerViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return false }
        
        let char = string.cString(using: String.Encoding.utf8)
        let isBackSpace = strcmp(char, "\\b")
        
        var tempValue = "\(text)\(string)"
        if (isBackSpace == -92) {
            tempValue.remove(at: tempValue.index(before: tempValue.endIndex))
        }
        self.viewModel?.enteredIPAddress(address: tempValue)
        return true
    }
}

extension IPCheckerViewController: Storyboarded {
    static var storyboardName: StoryboardName = .main
}
