//
//  IPCheckerViewModel.swift
//  IPChecker
//
//  Created by OLEKSANDR ISAIEV on 22.12.2020.
//

import Foundation

typealias EmptyCallback = (() -> ()?)
typealias StringCallback = ((String) -> ()?)

final class IPCheckerViewModel {
    
    private var ipAdress = ""
    var inactiveState: EmptyCallback?
    var readyState: EmptyCallback?
    var makeRequestState: EmptyCallback?
    var showResultState: StringCallback?
    var errorState: EmptyCallback?

    func load() {
        makeRequestState?()
        let ipClient = IPClient(ipAdress: ipAdress)
        let loader = RemoteLoader(client: ipClient)
        loader.load { [weak self] (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let model):
                    let string = "\(model.countryCode) - \(model.country), \(model.regionName) - \(model.region), \(model.city), \(model.zip)"
                    self?.showResultState?(string)

                case .failure(_):
                    self?.errorState?()
                    break
                }
            }
        }
    }
    
    func enteredIPAddress(address: String) {
        ipAdress = address
        print(ipAdress)
        if validateIpAddress(ipToValidate: address) {
            self.readyState?()
        } else {
            self.inactiveState?()
        }
    }

    private func validateIpAddress(ipToValidate: String) -> Bool {
        
        var sin = sockaddr_in()
        var sin6 = sockaddr_in6()

        if ipToValidate.withCString({ cstring in inet_pton(AF_INET6, cstring, &sin6.sin6_addr) }) == 1 {
            // IPv6 peer.
            return true
        }
        else if ipToValidate.withCString({ cstring in inet_pton(AF_INET, cstring, &sin.sin_addr) }) == 1 {
            // IPv4 peer.
            return true
        }

        return false
    }
}
