//
//  RemoteLoaderTest.swift
//  IPCheckerTests
//
//  Created by OLEKSANDR ISAIEV on 24.12.2020.
//

import XCTest
import IPChecker

class RemoteLoaderTest: XCTestCase {

    func test_init_doesNotRequestDataFromURL() {
        let (_, client) = makeSUT()
        XCTAssertTrue(client.requestedURLs.isEmpty)
    }

    func test_load_requestDataFromURL() {
        let url = URL(string: "http://new-url.com")!
        let (sut, client) = makeSUT(url: url)
        sut.load() { _ in }
        XCTAssertEqual(client.requestedURLs, [url])
    }
    
    func test_loadTwice_requestDataFromURLTwice() {
        let url = URL(string: "http://new-url.com")!
        let (sut, client) = makeSUT(url: url)
        sut.load() { _ in }
        sut.load() { _ in }
        
        XCTAssertEqual(client.requestedURLs, [url, url])
    }
    
    func test_load_deliversErrorOnClientError() {
        let (sut, client) = makeSUT()
        
        expect(sut, toCompleteWithError: .connectivity) {
            client.complete(with: .connectivity)
        }
    }
    
    
    // MARK: - Helpers
    
    private func makeSUT(url: URL = URL(string: "http://url.com")!) -> (sut: RemoteLoader, clietn: HTTPClientSPY) {
        let client = HTTPClientSPY(url: url)
        let sut = RemoteLoader(client: client)
        return (sut, client)
    }
    
    private func expect(_ sut: RemoteLoader, toCompleteWithError error: Error, when action: () -> (), file: StaticString = #filePath, line: UInt = #line) {
        
        var capturedResults = [RemoteLoader.Result]()
        sut.load() { capturedResults.append($0) }
        
        action()
        
        XCTAssertEqual(capturedResults, [.failure(error)], file: file, line: line)
    }
    
    private class HTTPClientSPY: HTTPClient {
        let url: URL?
        
        private var messages = [(url: URL, completion: (HTTPClientResult) -> Void)]()
        
        var requestedURLs: [URL] {
            return messages.map { $0.url }
        }
        
        internal init(url: URL) {
            self.url = url
        }
        
        func get(completion: @escaping (HTTPClientResult) -> ()) {
            messages.append((url!, completion))
        }
        
        func complete(with error: Error, at index: Int = 0) {
            messages[index].completion(.failure(error))
        }
        
    }
}
